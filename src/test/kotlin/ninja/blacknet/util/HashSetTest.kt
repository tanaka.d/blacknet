/*
 * Copyright (c) 2020 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.util

import kotlin.test.Test
import kotlin.test.assertEquals

class HashSetTest {
    @Test
    fun int() {
        val set = HashSet<Int>()
        assertEquals(set.size, 0)
        set.add(32)
        assertEquals(set.size, 1)
        set.add(32)
        assertEquals(set.size, 1)
    }

    @Test
    fun bytearray() {
        val set = HashSet<ByteArray>()
        assertEquals(set.size, 0)
        set.add(ByteArray(32))
        assertEquals(set.size, 1)
        set.add(ByteArray(32))
        assertEquals(set.size, 1)
    }
}
